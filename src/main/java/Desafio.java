import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Desafio {

    private WebDriver driver;

    @Before
    public void main(){
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");


    }

    @Test
    public void fluxoPrincipal(){

        driver.findElement(By.name("username")).sendKeys("Cleanne Silva");

        driver.findElement(By.name("password")).sendKeys("123456");

        WebElement commentsBox = driver.findElement(By.name("comments"));
        commentsBox.clear();
        commentsBox.sendKeys("Seja bem vindo ao dasafio!");

        WebElement checkbox = driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[5]/td/input[1]"));
        checkbox.click();

        WebElement checkbox2 = driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[5]/td/input[2]"));
        checkbox2.click();

        WebElement checkbox3 = driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[5]/td/input[3]]"));
        checkbox3.click();

        WebElement radio = driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[6]/td/input[2]"));
        radio.click();

        WebElement multipleSelectValue = driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[7]/td/select/option[4]"));
        multipleSelectValue.click();

        WebElement dropdown= driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[8]/td/select/option[3]"));
        dropdown.click();

        WebElement submit= driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[9]/td/input[2]"));
        submit.click();

        //Validações
        Assert.assertEquals("Cleanne Silva", driver.findElement(By.id("_valueusername")).getText());
        Assert.assertEquals("123456", driver.findElement(By.id("_valuepassword")).getText());
        Assert.assertEquals("Seja bem vindo ao dasafio!", driver.findElement(By.id("_valuecoments")).getText());
        Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
        Assert.assertEquals("cb2", driver.findElement(By.id("_valuecheckboxes1")).getText());
        Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes2")).getText());
        Assert.assertEquals("rd2", driver.findElement(By.id("_valueradioval")).getText());
        Assert.assertEquals("ms4", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Assert.assertEquals("dd3", driver.findElement(By.id("_valuedropdown")).getText());
        Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());

    }

}

